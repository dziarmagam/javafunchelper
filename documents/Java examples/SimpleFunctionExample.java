import java.util.Arrays;
import java.util.List;
import java.util.function.Function;



public class SimpleFunctionExample {
	
	static class UpperCaseTransformer implements Function<String, String>{
		@Override
		public String apply(String word) {
			return word.toUpperCase();
		}
	}
	

	public static void main(String[] args) {
		List<String> words = Arrays.asList("Printing", "Consumer");
		Function<String,String> printingConsumer = new UpperCaseTransformer();
		
		for (String word : words) {
			System.out.println(printingConsumer.apply(word));
		}
	}

}
