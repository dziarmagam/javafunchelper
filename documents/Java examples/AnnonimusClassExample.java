import java.util.function.Predicate;

public class AnnonimusClassExample {

//	public static void checkUserData(String login, String password) {
//		boolean isDataValid = new ChainValidator().withValidation(new Predicate<String>() {
//
//			@Override
//			public boolean test(String login) {
//				return !login.contains("%") && !login.contains("!");
//			}
//
//		}, login).withValidation(new Predicate<String>() {
//
//			@Override
//			public boolean test(String password) {
//				return password.contains("%") && password.contains("!");
//			}
//		}, password).validate();
//	}
	
	public static void checkUserData(String login, String password) {
		boolean isDataValid = new ChainValidator()
		.withValidation(loginArg -> !loginArg.contains("%") && !loginArg.contains("!"), login)
		.withValidation(passwordArg -> passwordArg.contains("%") && passwordArg.contains("!"), password)
		.validate();
	}
	

	private static class ChainValidator {
		private boolean result = true;

		public <T> ChainValidator withValidation(Predicate<T> validator, T testValue) {
			result = result && validator.test(testValue);
			return this;
		}

		public boolean validate() {
			return result;
		}
	}

	private static String getPassowrd() {
		// TODO Auto-generated method stub
		return null;
	}

	private static String getLogin() {
		// TODO Auto-generated method stub
		return null;
	}
}
