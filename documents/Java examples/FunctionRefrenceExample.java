import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;

public class FunctionRefrenceExample {

//	public static void main(String[] args) {
//		List<String> firstList = Arrays.asList("Ala", " ma");
//		List<String> secondList = Arrays.asList(" kota");
//		runOpperation( , firstList, secondList);
//	}
	public static void main(String[] args) {
		List<String> firstList = Arrays.asList("Ala", " ma");
		List<String> secondList = Arrays.asList(" kota");
		runOpperation(FunctionRefrenceExample::mergeLists, firstList, secondList);
	}

	public static <T> void runOpperation(
			BiFunction<List<T>, List<T>, List<? super T>> function,
			List<T> firstList,
			List<T> secondList) {
		List<? super T> result = function.apply(firstList, secondList);
		result.forEach(System.out::print);
	}

	static  BiFunction<List<String>, List<String>, List<String>> mergeListsFunction = 
			(firstList,secondList) ->{
				List<String> data = new ArrayList<>();
				data.addAll(firstList);
				data.addAll(secondList);
				return data;
			};
	
	public static <T> List<? super T> mergeLists(List<T> firstList, List<T> secondList) {
		List<? super T> data = new ArrayList<>();
		data.addAll(firstList);
		data.addAll(secondList);
		return data;
	}
	
	

}
