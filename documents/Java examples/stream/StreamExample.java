package stream;

import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamExample {
	public void printOnlyWordStartingWithA(List<String> words) {
		words.stream().filter(word -> word.startsWith("A") || word.startsWith("a")).forEach(System.out::println);
	}

	public static void main(String[] args) {
		int[] primitiveData = IntStream.range(0, 500000).toArray();
		Integer[] objectData = IntStream.range(0, 500000).boxed().toArray(Integer[]::new);
		int valuePrimitive = 0;
		Integer valueBoxed = 0;

		long startTime = System.currentTimeMillis();
		int interation = 100;

		for (int i = 0; i < interation; i++) {
			valuePrimitive = calulcateWithPrimivie(primitiveData);
		}
		System.out.println(System.currentTimeMillis() - startTime);

		startTime = System.currentTimeMillis();
		for (int i = 0; i < interation; i++) {
			valueBoxed = calulcateWithObjects(objectData);
		}

		System.out.println(System.currentTimeMillis() - startTime);
		System.out.println(valuePrimitive + " " + valueBoxed);
	}

	private static int calulcateWithPrimivie(int[] primitiveData) {
		return IntStream.of(primitiveData)
				.parallel()
				.map(v -> --v)
				.map(v -> --v)
				.map(v -> ++v)
				.sum();
	}

	private static int calulcateWithObjects(Integer[] objectData) {
		return Stream.of(objectData)
				.parallel()
				.map(v -> --v)
				.map(v -> --v)
				.map(v -> ++v)
				.reduce(0, Integer::sum);
	}
}
