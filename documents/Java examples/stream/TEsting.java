package stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TEsting {

	public static void main(String[] args) {
		List<String> list = new ArrayList<>(Arrays.asList("one", "two"));
		Stream<String> stream = list.stream();
		String value = stream.map(string -> {
					list.add("three");
					return string;
				})
				.collect(Collectors.joining(" "));
		System.out.println(value);
	}

}
