package stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamBoundries {

	public void concurrentModification(){
		List<String> list = new ArrayList<>(Arrays.asList("one", "two"));
		Stream<String> stream = list.stream();
		String value = stream.map(string -> {
					list.add("three");
					return string;
				})
				.collect(Collectors.joining(" "));
		System.out.println(value);
	}
	
}
