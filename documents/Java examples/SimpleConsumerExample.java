import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class SimpleConsumerExample {

	static class PrintingConsumer implements Consumer<String> {
		@Override
		public void accept(String message) {
			System.out.println(message);
		}
	}
	
	public static void main(String[] args){
		List<String> words = Arrays.asList("Printing", "Consumer");
		Consumer<String> printingConsumer = new PrintingConsumer();
		
		for (String word : words) {
			printingConsumer.accept(word);
		}
	}

}
