import java.util.Random;
import java.util.function.IntSupplier;

public class SimpleSupplierExample {

	private static class RandomNumberSupplier implements IntSupplier {
		@Override
		public int getAsInt() {
			return new Random().nextInt();
		}
	}

	public static void main(String[] args) {
		IntSupplier randomNumberSupplier = new RandomNumberSupplier();

		for (int i = 0; i < 5; i++) {
			System.out.println(randomNumberSupplier.getAsInt());
		}
	}

}
