
@FunctionalInterface
public interface FunctionalInterfecaExample{
	void process();
	default void defaultFunction() {
		System.out.println("Default behavior");
	}
}

//class DoubleInterfaceImplementation implements A, B{
//	
//	@Override
//	public void defaultFunction(){
//		
//	}
//}

interface C extends A, B{
	@Override
	public default void defaultFunction(){
		//Odwo�anie do metody zaimplementowanej w A
		A.super.defaultFunction();
		//Odwo�anie do metody zaimplementowanej w B
		B.super.defaultFunction();
	}
}

interface A{
	default void defaultFunction(){
		
	}
	void nonDefaultMethod();
}

interface B{
	default void defaultFunction(){
		
	}
	void nonDefaultMethod();
}