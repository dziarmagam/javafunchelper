package func.helper.diffcoding;

import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class DiffCodingUtilsTests {

	@Test
	public void giveSimpleInput_whenLeftDiffCoding_thenReturnCorrectDiff() {
		// Given
		List<Integer> simpleInput = Arrays.asList(1, 2, 3, 4, 5);
		List<Integer> expectedResult = Arrays.asList(1, 3, 5, 7, 9);
		// When
		List<Integer> result = DiffCodingUtils.leftDiffCoding(simpleInput, (a1, a2) -> a1 + a2).collect(toList());

		// Then
		assertEquals(expectedResult, result);
	}

	@Test
	public void giveSimpleInput_whenRightDiffCoding_thenReturnCorrectDiff() {
		// Given
		List<Integer> simpleInput = Arrays.asList(1, 2, 3, 4, 5);
		List<Integer> expectedResult = Arrays.asList(3, 5, 7, 9, 5);
		// When
		List<Integer> result = DiffCodingUtils.rightDiffCoding(simpleInput, (a1, a2) -> a1 + a2).collect(toList());

		// Then
		assertEquals(expectedResult, result);
	}

}
