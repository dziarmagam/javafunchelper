package func.helper.stream.window;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

public class CoordinateTest {

	@Test
	public void givenRangeX_shouldCreateCoordinateInRow() {
		// Given
		int start = 0;
		int end = 6;
		int row = 1;

		// When
		Coordinate[] evaluate = Coordinate.ofRangeX(start, end, row).evaluate();
		System.out.println(Arrays.toString(evaluate));

		// Then
		for (int i = 0; i < evaluate.length; i++) {
			Assert.assertTrue(evaluate[i].x == i);
			Assert.assertTrue(evaluate[i].y == row);
		}

	}

	@Test
	public void givenRangeY_shouldCreateCoordinateInCollumn() {
		// Given
		int start = 0;
		int end = 6;
		int collumn1 = 1;
		int collumn2 = 2;

		// When
		Coordinate[] evaluate = Coordinate.ofRangeY(start, end, collumn1)
				.andRangeY(start, end, collumn2)
				.evaluate();
	
		System.out.println(Arrays.toString(evaluate));

		// Then
		for (int j = 0; j < 5; j++) {
			for (int i = 0; i < 2; i++) {
				Assert.assertTrue(evaluate[i + (j * 2)].x == i+1);
				Assert.assertTrue(evaluate[i + (j * 2)].y == j);
			}
		}

	}
	
	@Test
	@Ignore
	public void givenRangeYX_shouldCreateCoordinateInCollumn() {
		// Given
		int start = 0;
		int end = 6;
		int collumn1 = 1;
		int collumn2 = 2;
		int row = 1;
		// When
		
		Coordinate[] rangeXY = Coordinate.ofRangeY(start, end, collumn1)
				.andRangeY(start, end, collumn2)
				.andRangeX(start, end, row)
				.evaluate();
		System.out.println(Arrays.toString(rangeXY));

		// Then
		for (int j = 0; j < 5; j++) {
			for (int i = 0; i < 2; i++) {
				Assert.assertTrue(rangeXY[i + (j * 2)].x == i+1);
				Assert.assertTrue(rangeXY[i + (j * 2)].y == j);
			}
		}

	}

}
