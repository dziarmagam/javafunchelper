package func.helper.stream.window;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Test;

import func.helper.stream.window.Coordinate.Range;

public class WindowFiltersTest {

	@Test
	public void givenCollumnFilter_whenFiltringStream_shouldReturnOnlyElementsInGivenCollumn() {
		//Given
		int expetedSize = 3;
		int matrixLength = expetedSize;
		Coordinate[] corrds = Coordinate.ofRangeX(0, matrixLength, 0).evaluate();
		List<Integer> data = Arrays.asList(1,2,3,4,5,6,7,8,9);
		
		//When
		List<Window<Integer, Integer>> collumn = WindowStreamUtils.createWindowStream(data, matrixLength,corrds)
		.filter(WindowFilters.columnFilter(0))
		.collect(Collectors.toList());
		
		//Then
		assertEquals(expetedSize, collumn.size());
		for (int i = 0; i < collumn.size(); i++) {
			assertEquals(i*3, collumn.get(i).center.index);
		}
	}
	
	@Test
	public void givenRowFilter_whenFiltringStream_shouldReturnOnlyElementsInGivenRow() {
		//Given
		int expetedSize = 3;
		int matrixLength = expetedSize;
		Coordinate[] corrds = Coordinate.ofRangeY(0, matrixLength, 0).evaluate();
		List<Integer> data = Arrays.asList(1,2,3,4,5,6,7,8,9);
		
		//When
		List<Window<Integer, Integer>> collumn = WindowStreamUtils.createWindowStream(data, matrixLength,corrds)
		.filter(WindowFilters.rowFilter(0))
		.collect(Collectors.toList());
		
		//Then
		assertEquals(expetedSize, collumn.size());
		for (int i = 0; i < collumn.size(); i++) {
			assertEquals(i, collumn.get(i).center.index);
		}
	}

}
