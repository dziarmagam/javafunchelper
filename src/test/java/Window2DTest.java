import static func.helper.stream.window.WindowStreamUtils.getCoordinatesForWindow;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;

import org.junit.Test;

import func.helper.stream.BaseExtendedStream;
import func.helper.stream.window.Coordinate;
import func.helper.stream.window.WindowStreamUtils;

public class Window2DTest {

	@Test
	public void givenIntegerList_whenWindowStreamIsCreatedFromCollection_thenCreatedWindowAreCorrect() {
		// Given
		int matrixLength = 3;
		List<Integer> collection = javaslang.collection.List.of(1, 2, 3, 4, 5, 6, 7, 8, 9).toJavaList();

		WindowStreamUtils.createWindowStream(collection, matrixLength, Coordinate.ofAll(0, 0, 0, 1)).forEach(
				System.out::println);
		WindowStreamUtils.createWindowStream(collection.stream(), matrixLength, Coordinate.ofAll(0, 0, 0, 1)).forEach(
				System.out::println);
	}

	@Test
	public void should_transform_image() throws IOException {
		// Given
		Path path = Paths.get("src/main/resources/noise.png");
		List<Integer> loadImage = readRGB(path);
		List<Integer> collect = BaseExtendedStream.of(loadImage.stream())
				.window(514, 5)
//				.parallel()
				.map(window -> {
					window.values.sort(Integer::compareTo);
					return window.values.get(window.values.size() / 2);
				}).collect(Collectors.toList());
		saveRgb("result.png", collect);

	}
	
	private Coordinate[] getCoordinateBaseOnInteger(Integer integer){
		int value = new Random().nextInt(20) * 2 + 1;
		return getCoordinatesForWindow(value);
	}

	public void saveRgb(String path, List<Integer> rgbs) throws IOException {

		BufferedImage image = new BufferedImage(514, 544, BufferedImage.TYPE_INT_RGB);

		int count = 0;
		for (int i = 0; i < 544; i += 1) {
			for (int j = 0; j < 514; j += 1) {
				// System.out.print(rgbs.get(j + (i*544)));
				image.setRGB(j, i, rgbs.get(j + (i * 514)));
			}
			// System.out.println();
		}

		File outputImage = new File(path);
		ImageIO.write(image, "png", outputImage);

	}

	private List<Integer> loadImage(Path path) {
		try {
			byte[] data2 = readData(path);
			InputStream inputStream = new ByteArrayInputStream(data2);

			List<Integer> bytes = new ArrayList<>();
			for (int i = 0; i < data2.length; i += 4) {
				byte[] integer = new byte[4];
				integer[0] = data2[i];
				integer[1] = data2[i + 1];
				integer[2] = data2[i + 2];
				integer[3] = data2[i + 3];
				// integer[4] = data2[i+4];
				// integer[5] = data2[i+5];
				// integer[6] = data2[i+6];
				// integer[7] = data2[i+7];
				// integer = integer | data2[i];
				// integer = integer << 8;
				// integer = integer | data2[i+1];
				// integer = integer << 8;
				// integer = integer | data2[i+2];
				// integer = integer << 8;
				// integer = integer | data2[i+3];
				// bytes.add(data2[i] | data2[i + 1] | data2[i + 2] | data2[i +
				// 3]);
				bytes.add(ByteBuffer.wrap(integer).order(ByteOrder.BIG_ENDIAN).getInt());
			}

			return bytes;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

	}

	private List<Integer> readRGB(Path path) throws IOException {
		BufferedImage bufferedImage = ImageIO.read(path.toFile());
		List<Integer> rgbs = new ArrayList<>();
		for (int i = 0; i < bufferedImage.getHeight(); i++) {
			for (int j = 0; j < bufferedImage.getWidth(); j++) {
				int rgb = bufferedImage.getRGB(j, i);
				// System.out.print(rgb);
				rgbs.add(rgb);
			}
			// System.out.println();
		}
		return rgbs;
	}

	private byte[] readDataImage(Path path) throws IOException {
		BufferedImage bufferedImage = ImageIO.read(path.toFile());
		// get DataBufferBytes from Raster

		// bufferedImage.getRGB(x, y);
		WritableRaster raster = bufferedImage.getRaster();
		DataBufferByte data = (DataBufferByte) raster.getDataBuffer();

		byte[] data2 = data.getData();
		return data2;
	}

	private byte[] readData(Path path) throws IOException {

		return java.nio.file.Files.readAllBytes(path);
	}

	private void saveImage(Path path, List<Integer> data) throws IOException {
		byte[] dataByte = new byte[data.size() * 4];

		for (int i = 0; i < dataByte.length; i += 4) {
			int integer = data.get(i / 4);

			dataByte[i] = (byte) (integer >> 24);
			dataByte[i + 1] = (byte) (integer >> 16);
			dataByte[i + 2] = (byte) (integer >> 8);
			dataByte[i + 3] = (byte) (integer /* >> 0 */);
		}

		FileOutputStream fileOutputStream = new FileOutputStream(new File("result.png"));
		fileOutputStream.write(dataByte);
		fileOutputStream.close();

	}

}
