package func.helper.tuple;

import java.util.Objects;

public class Tuple5<A, B, C, D, E> {
	public final A value1;
	public final B value2;
	public final C value3;
	public final D value4;
	public final E value5;

	public Tuple5(A value1, B value2, C value3, D value4, E value5) {
		super();
		this.value1 = value1;
		this.value2 = value2;
		this.value3 = value3;
		this.value4 = value4;
		this.value5 = value5;
	}

	@Override
	public int hashCode() {
		return Objects.hash(value1, value2, value3, value4, value5);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tuple5 other = (Tuple5) obj;
		return Objects.equals(this.value1, other.value1) && Objects.equals(this.value2, other.value2)
				&& Objects.equals(this.value3, other.value3) && Objects.equals(this.value4, other.value4)
				&& Objects.equals(this.value5, other.value5);
	}
}