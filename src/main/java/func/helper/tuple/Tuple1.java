package func.helper.tuple;

import java.util.Objects;

public class Tuple1<A> {
	public final A value1;

	public Tuple1(A value1) {
		super();
		this.value1 = value1;
	}

	@Override
	public int hashCode() {
		return Objects.hash(value1);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tuple1 other = (Tuple1) obj;
		return Objects.equals(this.value1, other.value1);
	}
}