package func.helper.tuple;

import java.util.Objects;

public class Tuple2<A, B> {
	public final A value1;
	public final B value2;

	public Tuple2(A value1, B value2) {
		super();
		this.value1 = value1;
		this.value2 = value2;
	}

	@Override
	public int hashCode() {
		return Objects.hash(value1, value2);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tuple2 other = (Tuple2) obj;
		return Objects.equals(this.value1, other.value1) && Objects.equals(this.value2, other.value2);
	}
}