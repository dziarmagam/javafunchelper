package func.helper.tuple;

import java.util.Objects;

public class Tuple9<A, B, C, D, E, F, G, H, I> {
	public final A value1;
	public final B value2;
	public final C value3;
	public final D value4;
	public final E value5;
	public final F value6;
	public final G value7;
	public final H value8;
	public final I value9;

	public Tuple9(A value1, B value2, C value3, D value4, E value5, F value6, G value7, H value8, I value9) {
		super();
		this.value1 = value1;
		this.value2 = value2;
		this.value3 = value3;
		this.value4 = value4;
		this.value5 = value5;
		this.value6 = value6;
		this.value7 = value7;
		this.value8 = value8;
		this.value9 = value9;
	}

	@Override
	public int hashCode() {
		return Objects.hash(value1, value2, value3, value4, value5, value6, value7, value8, value9);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tuple9 other = (Tuple9) obj;
		return Objects.equals(this.value1, other.value1) && Objects.equals(this.value2, other.value2)
				&& Objects.equals(this.value3, other.value3) && Objects.equals(this.value4, other.value4)
				&& Objects.equals(this.value5, other.value5) && Objects.equals(this.value6, other.value6)
				&& Objects.equals(this.value7, other.value7) && Objects.equals(this.value8, other.value8)
				&& Objects.equals(this.value9, other.value9);
	}
}