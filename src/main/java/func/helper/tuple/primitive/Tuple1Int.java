package func.helper.tuple.primitive;
import java.util.Objects;

public class Tuple1Int{public final int value1;
public Tuple1Int(int value1) { 
super(); 
 this.value1 = value1;
}
@Override 
 public int hashCode() {
return Objects.hash(value1);
}@Override 
 public boolean equals(Object obj){
if (this == obj)
	return true; 
if (obj == null) 
	return false; 
if (getClass() != obj.getClass()) 
	return false; 
Tuple1Int other = (Tuple1Int) obj;

return Objects.equals(this.value1,other.value1);
}}