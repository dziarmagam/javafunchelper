package func.helper.tuple.primitive;
import java.util.Objects;

public class Tuple3IntDoubleDouble{public final int value1;
public final double value2;
public final double value3;
public Tuple3IntDoubleDouble(int value1, double value2, double value3) { 
super(); 
 this.value1 = value1;
this.value2 = value2;
this.value3 = value3;
}
@Override 
 public int hashCode() {
return Objects.hash(value1, value2, value3);
}@Override 
 public boolean equals(Object obj){
if (this == obj)
	return true; 
if (obj == null) 
	return false; 
if (getClass() != obj.getClass()) 
	return false; 
Tuple3IntDoubleDouble other = (Tuple3IntDoubleDouble) obj;

return Objects.equals(this.value1,other.value1) && 
Objects.equals(this.value2,other.value2) && 
Objects.equals(this.value3,other.value3);
}}