package func.helper.tuple.primitive;
import java.util.Objects;

public class Tuple1Double{public final double value1;
public Tuple1Double(double value1) { 
super(); 
 this.value1 = value1;
}
@Override 
 public int hashCode() {
return Objects.hash(value1);
}@Override 
 public boolean equals(Object obj){
if (this == obj)
	return true; 
if (obj == null) 
	return false; 
if (getClass() != obj.getClass()) 
	return false; 
Tuple1Double other = (Tuple1Double) obj;

return Objects.equals(this.value1,other.value1);
}}