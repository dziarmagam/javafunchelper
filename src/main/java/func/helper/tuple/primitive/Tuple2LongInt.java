package func.helper.tuple.primitive;
import java.util.Objects;

public class Tuple2LongInt{public final long value1;
public final int value2;
public Tuple2LongInt(long value1, int value2) { 
super(); 
 this.value1 = value1;
this.value2 = value2;
}
@Override 
 public int hashCode() {
return Objects.hash(value1, value2);
}@Override 
 public boolean equals(Object obj){
if (this == obj)
	return true; 
if (obj == null) 
	return false; 
if (getClass() != obj.getClass()) 
	return false; 
Tuple2LongInt other = (Tuple2LongInt) obj;

return Objects.equals(this.value1,other.value1) && 
Objects.equals(this.value2,other.value2);
}}