package func.helper.tuple.primitive;
import java.util.Objects;

public class Tuple4LongDoubleIntInt{public final long value1;
public final double value2;
public final int value3;
public final int value4;
public Tuple4LongDoubleIntInt(long value1, double value2, int value3, int value4) { 
super(); 
 this.value1 = value1;
this.value2 = value2;
this.value3 = value3;
this.value4 = value4;
}
@Override 
 public int hashCode() {
return Objects.hash(value1, value2, value3, value4);
}@Override 
 public boolean equals(Object obj){
if (this == obj)
	return true; 
if (obj == null) 
	return false; 
if (getClass() != obj.getClass()) 
	return false; 
Tuple4LongDoubleIntInt other = (Tuple4LongDoubleIntInt) obj;

return Objects.equals(this.value1,other.value1) && 
Objects.equals(this.value2,other.value2) && 
Objects.equals(this.value3,other.value3) && 
Objects.equals(this.value4,other.value4);
}}