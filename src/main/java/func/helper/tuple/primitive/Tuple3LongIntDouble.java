package func.helper.tuple.primitive;
import java.util.Objects;

public class Tuple3LongIntDouble{public final long value1;
public final int value2;
public final double value3;
public Tuple3LongIntDouble(long value1, int value2, double value3) { 
super(); 
 this.value1 = value1;
this.value2 = value2;
this.value3 = value3;
}
@Override 
 public int hashCode() {
return Objects.hash(value1, value2, value3);
}@Override 
 public boolean equals(Object obj){
if (this == obj)
	return true; 
if (obj == null) 
	return false; 
if (getClass() != obj.getClass()) 
	return false; 
Tuple3LongIntDouble other = (Tuple3LongIntDouble) obj;

return Objects.equals(this.value1,other.value1) && 
Objects.equals(this.value2,other.value2) && 
Objects.equals(this.value3,other.value3);
}}