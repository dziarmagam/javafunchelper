package func.helper.stream.window;

import java.util.List;

import func.helper.stream.Indexed;

public class Window<T, R> {
	public final List<T> values;
	public final Indexed<R> center;
	public final int streamWidth;

	public Window(List<T> values, Indexed<R> center, int streamWidth) {
		super();
		this.values = values;
		this.center = center;
		this.streamWidth = streamWidth;
	}

	public static <T, R> Window<T, R> of(List<T> values, Indexed<R> center, int width) {
		return new Window<T, R>(values, center, width);
	}

	@Override
	public String toString() {
		return "Window [values=" + values + ", center=" + center + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((center == null) ? 0 : center.hashCode());
		result = prime * result + ((values == null) ? 0 : values.hashCode());
		return result;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Window other = (Window) obj;
		if (center == null) {
			if (other.center != null)
				return false;
		} else if (!center.equals(other.center))
			return false;
		if (values == null) {
			if (other.values != null)
				return false;
		} else if (!values.equals(other.values))
			return false;
		return true;
	}

}
