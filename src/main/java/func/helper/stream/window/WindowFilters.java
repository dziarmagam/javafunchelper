package func.helper.stream.window;

import java.util.function.Predicate;

/**
 * 
 * Class holding filters for windowed stream
 *
 */
public final class WindowFilters {
	public static Predicate<Window<?,?>> columnFilter(int collumn){
		return window -> window.center.index % window.streamWidth == collumn;
	}
	
	public static Predicate<Window<?,?>> rowFilter(int row){
		return window -> window.center.index >= window.streamWidth * row && window.center.index < window.streamWidth * (row + 1);
	}
	
}
