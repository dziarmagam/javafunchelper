package func.helper.stream.window;

import static func.helper.stream.window.WindowStreamUtils.findWindow;

import java.util.ArrayList;
import java.util.List;
import java.util.Spliterator;
import java.util.function.Consumer;

import func.helper.stream.Indexed;

public class WindowSpliterator<T extends Window<S, S>, S> implements Spliterator<T> {

	private final List<S> elements;
	private final int streamWidth;
	private final Coordinate[] coordinates;
	private final Spliterator<S> sourceSpliterator;
	
	private boolean endOfSource = false;
	private int index = 0;

	public WindowSpliterator(int matrixLength, Coordinate[] coordinates, Spliterator<S> sourceSpliterator) {
		super();
		this.elements = new ArrayList<>();
		this.streamWidth = matrixLength;
		this.coordinates = coordinates;
		this.sourceSpliterator = sourceSpliterator;
	}

	@Override
	public boolean tryAdvance(Consumer<? super T> action) {
	    if (bufferToWindow(index) || index < elements.size()) // --> bufferToWindow
        {
          advance(action);
          return true;
        }
        return false;
	}

	@SuppressWarnings("unchecked")
	private void advance(Consumer<? super T> action) {
		Window<S, S> findWindow = findWindow(new Indexed<S>(elements.get(index), index), streamWidth, elements,
				coordinates);
		action.accept((T) findWindow);
		index++;
	}

	@Override
	public Spliterator<T> trySplit() {
		Spliterator<S> trySplit = sourceSpliterator.trySplit();
		if (trySplit != null) {
			return new WindowSpliterator<>(streamWidth,coordinates,trySplit);
		}
		return null;
	}

	@Override
	public long estimateSize() {
		return sourceSpliterator.estimateSize();
	}

	@Override
	public int characteristics() {
		return 0;
	}

	private boolean bufferToWindow(int index) {
		for (Coordinate coordinate : coordinates) {
			int indexFromCoord = WindowStreamUtils.getIndexFromCoord(index, streamWidth, coordinate);
			while (indexFromCoord > elements.size() - 1) {
				boolean tryAdvance = sourceSpliterator.tryAdvance(s -> elements.add(s));
				if (!tryAdvance) {
					return false;
				}
			}
		}
		return true;
	}

}
