package func.helper.stream.window;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.google.common.base.Preconditions;

import func.helper.stream.BaseExtendedStream;
import func.helper.stream.ExtendedStream;
import func.helper.stream.Indexed;

public class WindowStreamUtils {

	public static <T> ExtendedStream<Window<T, T>> createWindowStream(Collection<T> collection, int width,
			Coordinate... coordinates) {
		List<T> arrayList = new ArrayList<>(collection);

		ExtendedStream<Window<T, T>> map = BaseExtendedStream.of(collection).enumerate()
				.map(indexed -> findWindow(indexed, width, arrayList, coordinates));

		return map;
	}

	public static <T> ExtendedStream<Window<T, T>> createWindowStream(Stream<T> stream, int width, Coordinate... coordinates) {
		WindowSpliterator<Window<T, T>, T> spliterator = new WindowSpliterator<Window<T, T>, T>(width,
				coordinates, stream.spliterator());
		Stream<Window<T, T>> windowStream = StreamSupport.stream(spliterator, false);
		ExtendedStream<Window<T, T>> map = BaseExtendedStream.of(windowStream);
		return map;
	}

	public static <T> ExtendedStream<Window<T, T>> createWindowStream(Collection<T> collection, int width, int windowSize) {
		Preconditions.checkArgument(windowSize % 2 == 1);
		Coordinate[] coords = getCoordinatesForWindow(windowSize);
		return createWindowStream(collection, width, coords);
	}

	public static <T> ExtendedStream<Window<T, T>> createWindowStream(Stream<T> stream, int width, int windowSize) {
		Preconditions.checkArgument(windowSize % 2 == 1);
		Coordinate[] coords = getCoordinatesForWindow(windowSize);
		return createWindowStream(stream, width, coords);
	}
	
	public static <T> ExtendedStream<Window<T, T>> createWindowStream(Stream<T> stream, int width, Function<T,Coordinate[]> coordinatesGenerator) {
		CoordinateGeneratedWindowSpliterator<Window<T,T>, T> spliterator = new CoordinateGeneratedWindowSpliterator<>(width, coordinatesGenerator, stream.spliterator());
		Stream<Window<T, T>> windowStream = StreamSupport.stream(spliterator, false);
		return BaseExtendedStream.of(windowStream);
	}
	
	public static <T> ExtendedStream<Window<T, T>> createWindowStream(Collection<T> collection, int width){
		
		
		return null;
	}

	public static Coordinate[] getCoordinatesForWindow(int windowSize) {
		int bound = windowSize / 2;
		Coordinate[] coords = new Coordinate[windowSize * windowSize];
		int index = 0;
		for (int i = -bound; i <= bound; i++) {
			for (int j = -bound; j <= bound; j++) {
				coords[index] = new Coordinate(j, i);
				++index;
			}
		}
		return coords;
	}

	public static <T> Window<T, T> findWindow(Indexed<T> indexed, int windowSize, List<T> collection,
			Coordinate... coords) {

		int index = indexed.index;
		List<T> windowList = new ArrayList<>();
		for (Coordinate coordinate : coords) {
			int collectionIndex = getIndexFromCoord(index, windowSize, collection, coordinate);
			if (collectionIndex == -1) {
				continue;
			}
			windowList.add(collection.get(collectionIndex));
		}

		return new Window<T, T>(windowList, indexed, windowSize);
	}

	public static int getIndexFromCoord(int index, int windowSize, List<?> collection, Coordinate coords) {
		int coordIndex = index + coords.x - (windowSize * coords.y);
		if (coordIndex >= collection.size() || coordIndex < 0) {
			return -1;
		}
		return coordIndex;
	}
	
	public static int getIndexFromCoord(int index, int windowSize, Coordinate coords) {
		int coordIndex = index + coords.x - (windowSize * coords.y);
		return coordIndex;
	}

}
