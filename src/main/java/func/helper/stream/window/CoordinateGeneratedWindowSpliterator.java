package func.helper.stream.window;

import static func.helper.stream.window.WindowStreamUtils.findWindow;

import java.util.ArrayList;
import java.util.List;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.Function;

import func.helper.stream.Indexed;

public class CoordinateGeneratedWindowSpliterator<T extends Window<S, S>, S> implements Spliterator<T> {

	private List<S> elements;
	private final int matrixLength;
	private final Function<S, Coordinate[]> coordinatesGenerator;
	private final Spliterator<S> sourceSpliterator;
	private boolean endOfSource = false;
	private int index = 0;

	public CoordinateGeneratedWindowSpliterator(int matrixLength, Function<S, Coordinate[]> coordinatesGenerator,
			Spliterator<S> sourceSpliterator) {
		super();
		this.elements = new ArrayList<>();
		this.matrixLength = matrixLength;
		this.coordinatesGenerator = coordinatesGenerator;
		this.sourceSpliterator = sourceSpliterator;
	}

	@Override
	public boolean tryAdvance(Consumer<? super T> action) {
		endOfSource = !bufferToWindow(index);
		if (endOfSource) {
			if (index < elements.size()) {
				advance(action);
				return true;
			} else {
				return false;
			}
		} else {
			advance(action);
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	private void advance(Consumer<? super T> action) {
		Window<S, S> findWindow = findWindow(new Indexed<S>(elements.get(index), index), matrixLength, elements,
				coordinatesGenerator.apply(elements.get(index)));
		action.accept((T) findWindow);
		index++;
	}

	@Override
	public Spliterator<T> trySplit() {
		Spliterator<S> trySplit = sourceSpliterator.trySplit();
		if(trySplit != null){
			return new CoordinateGeneratedWindowSpliterator<>(matrixLength, coordinatesGenerator, trySplit);
		}
		return null;
	}

	@Override
	public long estimateSize() {
		return sourceSpliterator.estimateSize();
	}

	@Override
	public int characteristics() {
		return 0;
	}

	private boolean bufferToWindow(int index) {
		boolean bufferToIndex = bufferToIndex(index);
		if(!bufferToIndex){
			return false;
		}
		for (Coordinate coordinate : coordinatesGenerator.apply(elements.get(index))) {
			int indexFromCoord = WindowStreamUtils.getIndexFromCoord(index, matrixLength, coordinate);
			boolean buffered = bufferToIndex(indexFromCoord);
			if(!buffered){
				return buffered;
			}
		}
		return true;
	}

	private boolean bufferToIndex(int index) {
		while(index > elements.size() - 1){
			boolean tryAdvance = sourceSpliterator.tryAdvance(s -> elements.add(s));
			if (!tryAdvance) {
				return false;
			}
		}
		return true;
	}

}
