package func.helper.stream.window;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javaslang.Tuple2;

import com.google.common.base.Preconditions;

import func.helper.stream.window.Coordinate.Range.RangeType;
import func.helper.tuple.Tuple3;

/**
 * 
 * Class representing coordiantes.
 *
 */
public class Coordinate implements Comparable<Coordinate> {
	public final int x;
	public final int y;

	public Coordinate(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}

	public static Coordinate of(int x, int y) {
		return new Coordinate(x, y);
	}

	public static Range ofRangeX(int startInclusiveX, int endExclusiveX, int y) {
		return new Range(startInclusiveX, endExclusiveX, y, RangeType.X);
	}

	public static Range ofRangeY(int startInclusiveY, int endExclusiveY, int x) {
		return new Range(startInclusiveY, endExclusiveY, x, RangeType.Y);
	}

	public static Coordinate[] ofAll(int... data) {
		Preconditions.checkArgument(data.length % 2 == 0);
		Coordinate[] coordinates = new Coordinate[data.length / 2];
		for (int i = 0; i < data.length; i += 2) {
			coordinates[i / 2] = new Coordinate(data[i], data[i + 1]);
		}
		return coordinates;
	}

	public static class Range {
		public static enum RangeType {
			X, Y;
		}

		List<Tuple3<Coordinate, Integer, RangeType>> ranges = new ArrayList<>();

		public Range(int startInclusive, int endExclusive, int xy, RangeType type) {
			ranges.add(new Tuple3<>(Coordinate.of(startInclusive, endExclusive), xy, type));
		}
		
		public Range andRangeX(int startInclusiveX, int endExclusiveX, int y) {
			return and(startInclusiveX, endExclusiveX, y, RangeType.X);
		}
		
		public Range andRangeY(int startInclusiveY, int endExclusiveY, int x) {
			return and(startInclusiveY, endExclusiveY, x, RangeType.Y);
		}
		
		Range and(int startInclusive, int endExclusive, int xy, RangeType type) {
			ranges.add(new Tuple3<>(Coordinate.of(startInclusive, endExclusive), xy, type));
			return this;
		}

		public Coordinate[] evaluate() {
			Set<Coordinate> coords = new TreeSet<>();
			for (Tuple3<Coordinate, Integer, RangeType> range : ranges) {
				coords.addAll(getCoodsFromRange(range));
			}
			return coords.toArray(new Coordinate[coords.size()]);
		}

		private Collection<? extends Coordinate> getCoodsFromRange(Tuple3<Coordinate, Integer, RangeType> range) {
			Set<Coordinate> coordinates = new TreeSet<>();
			for (int i = range.value1.x; i < range.value1.y; i++) {
				if (range.value3 == RangeType.X) {
					coordinates.add(Coordinate.of(i, range.value2));
				} else {
					coordinates.add(Coordinate.of(range.value2, i));
				}
			}
			return coordinates;
		}

	}

	@Override
	public String toString() {
		return "Coordinate [x=" + x + ", y=" + y + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Coordinate other = (Coordinate) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}

	@Override
	public int compareTo(Coordinate o) {
		int compare = Integer.compare(this.y, o.y);

		if (compare == 0) {
			return Integer.compare(this.x, o.x);
		}

		return compare;
	}

}
