package func.helper.stream;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Optional;
import java.util.Spliterator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;
import java.util.stream.Collector;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import func.helper.stream.window.Coordinate;
import func.helper.stream.window.Window;
import func.helper.stream.window.WindowStreamUtils;

public class BaseExtendedStream<T> implements ExtendedStream<T> {

	private Stream<T> stream;

	private BaseExtendedStream(Stream<T> stream) {
		super();
		this.stream = stream;
	}

	public static <T> ExtendedStream<T> of(Collection<T> collection) {
		return new BaseExtendedStream<T>(collection.stream());
	}

	public static <T> ExtendedStream<T> of(Stream<T> stream) {
		return new BaseExtendedStream<T>(stream);
	}

	@Override
	public Iterator<T> iterator() {
		return stream.iterator();
	}

	@Override
	public Spliterator<T> spliterator() {
		return stream.spliterator();
	}

	@Override
	public boolean isParallel() {
		return stream.isParallel();
	}

	@Override
	public ExtendedStream<T> sequential() {
		this.stream = stream.sequential();
		return this;
	}

	@Override
	public ExtendedStream<T> parallel() {
		this.stream = stream.parallel();
		return this;
	}

	@Override
	public ExtendedStream<T> unordered() {
		this.stream = stream.unordered();
		return this;
	}

	@Override
	public ExtendedStream<T> onClose(Runnable closeHandler) {
		this.stream = stream.onClose(closeHandler);
		return this;
	}

	@Override
	public void close() {
		stream.close();
	}

	@Override
	public ExtendedStream<T> filter(Predicate<? super T> predicate) {
		this.stream = stream.filter(predicate);
		return this;
	}

	@Override
	public <R> ExtendedStream<R> map(Function<? super T, ? extends R> mapper) {
		return BaseExtendedStream.of(stream.map(mapper));
	}

	@Override
	public IntStream mapToInt(ToIntFunction<? super T> mapper) {
		return stream.mapToInt(mapper);
	}

	@Override
	public LongStream mapToLong(ToLongFunction<? super T> mapper) {
		return stream.mapToLong(mapper);
	}

	@Override
	public DoubleStream mapToDouble(ToDoubleFunction<? super T> mapper) {
		return stream.mapToDouble(mapper);
	}

	@Override
	public <R> ExtendedStream<R> flatMap(Function<? super T, ? extends Stream<? extends R>> mapper) {
		return BaseExtendedStream.of(stream.flatMap(mapper));
	}

	@Override
	public IntStream flatMapToInt(Function<? super T, ? extends IntStream> mapper) {
		return stream.flatMapToInt(mapper);
	}

	@Override
	public LongStream flatMapToLong(Function<? super T, ? extends LongStream> mapper) {
		return stream.flatMapToLong(mapper);
	}

	@Override
	public DoubleStream flatMapToDouble(Function<? super T, ? extends DoubleStream> mapper) {
		return stream.flatMapToDouble(mapper);
	}

	@Override
	public ExtendedStream<T> distinct() {
		this.stream = stream.distinct();
		return this;
	}

	@Override
	public ExtendedStream<T> sorted() {
		this.stream = stream.sorted();
		return this;
	}

	@Override
	public ExtendedStream<T> sorted(Comparator<? super T> comparator) {
		this.stream = stream.sorted(comparator);
		return this;
	}

	@Override
	public ExtendedStream<T> peek(Consumer<? super T> action) {
		this.stream = stream.peek(action);
		return this;
	}

	@Override
	public ExtendedStream<T> limit(long maxSize) {
		this.stream = stream.limit(maxSize);
		return this;
	}

	@Override
	public ExtendedStream<T> skip(long n) {
		this.stream = stream.skip(n);
		return this;
	}

	@Override
	public void forEach(Consumer<? super T> action) {
		stream.forEach(action);
	}

	@Override
	public void forEachOrdered(Consumer<? super T> action) {
		stream.forEachOrdered(action);
	}

	@Override
	public Object[] toArray() {
		return stream.toArray();
	}

	@Override
	public <A> A[] toArray(IntFunction<A[]> generator) {
		return stream.toArray(generator);
	}

	@Override
	public T reduce(T identity, BinaryOperator<T> accumulator) {
		return stream.reduce(identity, accumulator);
	}

	@Override
	public Optional<T> reduce(BinaryOperator<T> accumulator) {
		return stream.reduce(accumulator);
	}

	@Override
	public <U> U reduce(U identity, BiFunction<U, ? super T, U> accumulator, BinaryOperator<U> combiner) {
		return stream.reduce(identity, accumulator, combiner);
	}

	@Override
	public <R> R collect(Supplier<R> supplier, BiConsumer<R, ? super T> accumulator, BiConsumer<R, R> combiner) {
		return stream.collect(supplier, accumulator, combiner);
	}

	@Override
	public <R, A> R collect(Collector<? super T, A, R> collector) {
		return stream.collect(collector);
	}

	@Override
	public Optional<T> min(Comparator<? super T> comparator) {
		return stream.min(comparator);
	}

	@Override
	public Optional<T> max(Comparator<? super T> comparator) {
		return stream.max(comparator);
	}

	@Override
	public long count() {
		return stream.count();
	}

	@Override
	public boolean anyMatch(Predicate<? super T> predicate) {
		return stream.anyMatch(predicate);
	}

	@Override
	public boolean allMatch(Predicate<? super T> predicate) {
		return stream.allMatch(predicate);
	}

	@Override
	public boolean noneMatch(Predicate<? super T> predicate) {
		return stream.noneMatch(predicate);
	}

	@Override
	public Optional<T> findFirst() {
		return stream.findFirst();
	}

	@Override
	public Optional<T> findAny() {
		return stream.findAny();
	}
	@Override
	public boolean matchN(Predicate<? super T> predicate, long n) {
		return stream.map(predicate::test).filter(t -> t).count() >= n;
	}

	@Override
	public <S, R> ExtendedStream<R> zip(Stream<S> stream, BiFunction<T, S, R> zipFunction) {
		Stream<R> zippedStream = StreamSupport.stream(new ZipSpliterator<T,S,R>(stream.spliterator(),
				this.stream.spliterator(), zipFunction), false);
		
		return BaseExtendedStream.of(zippedStream);
	}

	@Override
	public ExtendedStream<Indexed<T>> enumerate() {
		final AtomicInteger index = new AtomicInteger(0);

		if (isParallel()) {
			return BaseExtendedStream
					.of(stream.sequential().map(t -> new Indexed<T>(t, index.getAndAdd(1))).parallel());
		} else {
			return BaseExtendedStream.of(stream.map(t -> new Indexed<T>(t, index.getAndAdd(1))));
		}
	}

	@Override
	public ExtendedStream<Indexed<T>> enumerate(Comparator<T> comaprator) {
		final AtomicInteger index = new AtomicInteger(0);

		if (isParallel()) {
			return BaseExtendedStream.of(stream.sorted(comaprator).sequential()
					.map(t -> new Indexed<T>(t, index.getAndAdd(1))).parallel());
		} else {
			return BaseExtendedStream.of(stream.sorted(comaprator).map(t -> new Indexed<T>(t, index.getAndAdd(1))));
		}
	}

	@Override
	public ExtendedStream<Window<T, T>> window(int matrixLength, Coordinate... coordinates) {
		return WindowStreamUtils.createWindowStream(this, matrixLength, coordinates);
	}

	@Override
	public ExtendedStream<Window<T, T>> window(int matrixLength, int windowSize) {
		return WindowStreamUtils.createWindowStream(this, matrixLength, windowSize);
	}

	@Override
	public ExtendedStream<Window<T, T>> window(int matrixLength, Function<T, Coordinate[]> coordinatesGenerator) {
		return WindowStreamUtils.createWindowStream(this, matrixLength, coordinatesGenerator);
	}
	
	

}
