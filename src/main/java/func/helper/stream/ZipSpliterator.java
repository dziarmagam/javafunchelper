package func.helper.stream;

import java.util.Spliterator;
import java.util.function.BiFunction;
import java.util.function.Consumer;

/**
 * 
 *Spliterator used to create stream zipping to other streams
 *
 */
public class ZipSpliterator<T, S, R> implements Spliterator<R> {

	private final Spliterator<S> rightSpliterator;
	private final Spliterator<T> leftSpliterator;
	private final BiFunction<T, S, R> zipFunction;
	private boolean rightHasNext = false;

	public ZipSpliterator(Spliterator<S> rightStream, Spliterator<T> leftStream, BiFunction<T, S, R> zipFunction) {
		super();
		this.rightSpliterator = rightStream;
		this.leftSpliterator = leftStream;
		this.zipFunction = zipFunction;
	}

	@Override
	public boolean tryAdvance(Consumer<? super R> action) {
		this.rightHasNext = false;
		boolean leftHasNext = leftSpliterator.tryAdvance(getLeftSpliteratorConsumer(action));
		return leftHasNext & rightHasNext;
	}

	private Consumer<? super T> getLeftSpliteratorConsumer(Consumer<? super R> action) {
		return t -> {
			rightSpliterator.tryAdvance(getRightSpliteratorCunsumer(action, t));
		};
	}

	private Consumer<? super S> getRightSpliteratorCunsumer(Consumer<? super R> action, T t) {
		return s -> {
			this.rightHasNext = true;
			action.accept(zipFunction.apply(t, s));
		};
	}

	@Override
	public Spliterator<R> trySplit() {
		return null;
	}

	@Override
	public long estimateSize() {
		return Math.min(leftSpliterator.estimateSize(), rightSpliterator.estimateSize());
	}

	@Override
	public int characteristics() {
		return leftSpliterator.characteristics() & rightSpliterator.characteristics()
				& ~(Spliterator.DISTINCT | Spliterator.SORTED);
	}

}
