package func.helper.stream;

import java.util.Comparator;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

import func.helper.stream.window.Coordinate;
import func.helper.stream.window.Window;

public interface ExtendedStream<T> extends Stream<T> {

	/**
	 * Method for checking if N elements in the stream match given predicate
	 * 
	 * @param predicate
	 * @param n
	 *            number of elements to match predicate
	 * @return true if n elements in the stream match {@code predicate}
	 */
	boolean matchN(Predicate<? super T> predicate, long n);

	/**
	 * Method for creating enumerated stream
	 * 
	 * @return Enumerated stream
	 */
	ExtendedStream<Indexed<T>> enumerate();

	/**
	 * Method for creating enumerated stream.
	 * <p>
	 * Stream is enumerated base on given comparator
	 * 
	 * @param comperator
	 *            {@link Comparator} for sorting stream
	 * @returnEnumerated stream
	 */
	ExtendedStream<Indexed<T>> enumerate(Comparator<T> comperator);

	/**
	 * Method for ziping current stream with another using given function
	 * 
	 * @param stream
	 *            {@link Stream} with which current stream will be zipped with
	 * @param zipFunction
	 *            zipping {@link Function}
	 * @return {@link Stream}
	 */
	<S, R> ExtendedStream<R> zip(Stream<S> stream, BiFunction<T, S, R> zipFunction);
	
	ExtendedStream<Window<T,T>> window(int matrixLength, Coordinate... coordinates);

	ExtendedStream<Window<T,T>> window(int matrixLength, int windowSize);

	ExtendedStream<Window<T,T>> window(int matrixLength, Function<T,Coordinate[]> coordinatesGenerator);

	// Overridden method to return ExtendedStream.
	@Override
	<R> ExtendedStream<R> map(Function<? super T, ? extends R> mapper);

	@Override
	ExtendedStream<T> skip(long n);

	@Override
	ExtendedStream<T> limit(long maxSize);

	@Override
	ExtendedStream<T> filter(Predicate<? super T> predicate);

	@Override
	ExtendedStream<T> distinct();

	@Override
	<R> ExtendedStream<R> flatMap(Function<? super T, ? extends Stream<? extends R>> mapper);

	@Override
	ExtendedStream<T> peek(Consumer<? super T> action);

	@Override
	ExtendedStream<T> unordered();

	@Override
	ExtendedStream<T> parallel();

	@Override
	ExtendedStream<T> sorted();

	@Override
	ExtendedStream<T> sorted(Comparator<? super T> comparator);

}
