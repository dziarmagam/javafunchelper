package func.helper.stream;

/**
 * 
 * Class representing indexed entity.
 * 
 */
public class Indexed<T> {

	public final T value;
	public final int index;

	public Indexed(T value, int index) {
		super();
		this.value = value;
		this.index = index;
	}

	@Override
	public String toString() {
		return "Indexed [value=" + value + ", index=" + index + "]";
	}
	
}
