package func.helper.diffcoding;

import java.util.Collection;
import java.util.function.BiFunction;
import java.util.stream.Stream;

import com.codepoetics.protonpack.StreamUtils;

import func.helper.stream.BaseExtendedStream;
import func.helper.stream.ExtendedStream;

public class DiffCodingUtils {

	@SuppressWarnings("unchecked")
	public static <T, R> Stream<R> leftDiffCoding(Collection<T> data, BiFunction<T, T, R> addFunction) {
		ExtendedStream<R> zip = BaseExtendedStream.of(data).skip(1)
				.zip(data.stream().limit(data.size() - 1), (a, b) -> addFunction.apply(a, b));

		return (Stream<R>) Stream.concat(data.stream().limit(1), zip);
	}

	@SuppressWarnings("unchecked")
	public static <T, R> Stream<R> rightDiffCoding(Collection<T> data, BiFunction<T, T, R> addFunction) {
		ExtendedStream<R> zip = BaseExtendedStream.of(data).limit(data.size() - 1)
				.zip(data.stream().skip(1), (a, b) -> addFunction.apply(a, b));
		return (Stream<R>) Stream.concat(zip, data.stream().skip(data.size() - 1));
	}
}
