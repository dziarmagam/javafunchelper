package func.helper.example;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.imageio.ImageIO;

import func.helper.stream.BaseExtendedStream;
import func.helper.stream.window.Coordinate;
import func.helper.stream.window.Window;
import func.helper.stream.window.WindowFilters;
import func.helper.tuple.primitive.Tuple4IntIntIntInt;

public class ImageProcessing {

	public static void main(String[] args) throws IOException {
//		new ImageProcessing().medianFilter(Paths.get("src/main/resources/noise.png"));
//		new ImageProcessing().meanFilter();
		new ImageProcessing().medianImperative(Paths.get("src/main/resources/noise.png"));

	}

	public void medianFilter(Path path) throws IOException {
		List<Integer> loadImage = readRGB(path);
		int imageWidth = 514;
		int imageHeight = 544;

		List<Integer> collect = BaseExtendedStream.of(loadImage.stream())
				.window(imageWidth, 5)
				.map(this::medianFilter)
				.collect(Collectors.toList());
		saveRgb("resultMedian.png", collect, imageWidth, imageHeight);
	}
	
	public void meanFilter() throws IOException {
		Path path = Paths.get("src/main/resources/noise.png");
		List<Integer> loadImage = readGray(path);
		int imageWidth = 514;
		int imageHeight = 544;

		List<Integer> collect = BaseExtendedStream.of(loadImage.stream())
				.window(imageWidth, 5)
				.map(this::meanFiltr)
				.collect(Collectors.toList());
		saveGray("resultMean.png", collect, imageWidth, imageHeight);
	}
	

	public void medianImperative(Path path) throws IOException {
		List<Integer> loadImage = readRGB(path);
		int imageWidth = 514;
		int imageHeight = 544;

		List<Integer> result = new ArrayList<>();
		for (int heightIndex = 0; heightIndex < imageHeight; heightIndex++) {
			for (int widthIndex = 0; widthIndex < imageWidth; widthIndex++) {
				int currentIndex = widthIndex + (heightIndex * imageWidth);
				int[] squareWindowIndexes = getSquareWindowIndexes(currentIndex, imageWidth, 5);
				List<Integer> window = new ArrayList<>();
				for (int i : squareWindowIndexes) {
					if (i >= 0 && i < imageHeight * imageWidth) {
						window.add(loadImage.get(i));
					}
				}
					window.sort(Integer::compare);
					result.add(window.get(window.size() / 2));
			}
		}

		saveRgb("resultMedian.png", result, imageWidth, imageHeight);
	}

	private int[] getSquareWindowIndexes(int currentIndex,int imageWidth, int windowSize) {
		int delta = windowSize / 2;
		int[] window = new int[windowSize*windowSize];
		int index = 0;
		for (int height = -delta; height <= delta; height++) {
			for (int width = -delta; width <= delta; width++) {
				window[index] = currentIndex + (height*imageWidth) + width;
				index++;
			}	
		}
		return window;
	}

	Integer medianFilter(Window<Integer, Integer> window){
		window.values.sort(Integer::compareTo);
		return window.values.get(window.values.size() / 2);
	}
	
	Integer meanFiltr(Window<Integer, Integer> window){
		return window.values.stream().reduce(Integer::sum).get() / window.values.size();
	}
//	
//	Integer meanFiltr(Window<Tuple4IntIntIntInt, Tuple4IntIntIntInt> window){
// 		AtomicInteger counter = new AtomicInteger(0);
//		List<Integer> rgba = BaseExtendedStream.of(window.values.stream()
//				.flatMap(this::flatTuple))
////				.peek( s -> {
////					if(counter.get() % 4 == 0)
////						System.out.println(s);
////					else
////						System.out.print(s + " ");
////					counter.incrementAndGet();
////				})
//				.window(4, Coordinate.ofRangeY(0, window.values.size(), 0).evaluate())
//				.filter(WindowFilters.rowFilter(window.values.size() -1))
////				.peek(s -> System.out.println(s.values))
//				.map( windowRGB -> (int)windowRGB.values.stream().mapToInt(i -> i).average().getAsDouble())
//				.collect(Collectors.toList());
//		
//		return toPixel(rgba);
//	}
	
	private Integer toPixel(List<Integer> rgba) {
		int pixel = 0x00000000;
		pixel = rgba.get(0) & 0x000000FF;
		pixel = ((rgba.get(1) << 8) & 0x0000FF00) | pixel;
		pixel = ((rgba.get(2) << 16) & 0x00FF0000) | pixel;
		pixel = ((rgba.get(3) << 24) & 0xFF000000) | pixel;
	return pixel;
}
	
	

	Stream<Integer> flatTuple(Tuple4IntIntIntInt tuple){
		return Arrays.asList(tuple.value1, tuple.value2, tuple.value3, tuple.value4)
				.stream();
	}
	

	private List<Integer> readRGB(Path path) throws IOException {
		BufferedImage bufferedImage = ImageIO.read(path.toFile());
		List<Integer> rgbs = new ArrayList<>();
		for (int i = 0; i < bufferedImage.getHeight(); i++) {
			for (int j = 0; j < bufferedImage.getWidth(); j++) {
				int rgb = bufferedImage.getRGB(j, i);
				rgbs.add(rgb);
			}
		}
		return rgbs;
	}
	
	private List<Integer> readGray(Path path) throws IOException {
		BufferedImage bufferedImage = ImageIO.read(path.toFile());
		List<Tuple4IntIntIntInt> rgbs = new ArrayList<>();
		for (int i = 0; i < bufferedImage.getHeight(); i++) {
			for (int j = 0; j < bufferedImage.getWidth(); j++) {
				int rgb = bufferedImage.getRGB(j, i);
				rgbs.add(toTuple(rgb));
			}
		}
		return rgbs.stream().map(this::toGray).collect(Collectors.toList());
	}
	
	private Integer toGray(Tuple4IntIntIntInt rgba){
		return (int)(0.2989*rgba.value1 + 0.5870*rgba.value2 + 0.1140*rgba.value3)*255;
	}
	
//	private Integer fromGray(Integer rgba){
//		
//		int r = (rgb)&0xFF;
//		int g = (rgb>>8)&0xFF;
//		int b = (rgb>>16)&0xFF;
//		int a = (rgb>>24)&0xFF;
//		return (int)(0.2989*rgba.value1 + 0.5870*rgba.value2 + 0.1140*rgba.value3)*255;
//	}
	
	private int grayToRGB(int gray) {
//		int gray = 0;
//		if(blackAndWhite){
//			gray = (int) (d*255);
//		}else{
//			gray = (int) d;
//		}
		
		int RGB = 255;
		for (int i = 0; i < 3; i++) {
			RGB = RGB << 8;
			RGB = RGB | gray;
		}
		return RGB;
	}

	private Tuple4IntIntIntInt toTuple(int rgb) {
		
		int r = (rgb)&0xFF;
		int g = (rgb>>8)&0xFF;
		int b = (rgb>>16)&0xFF;
		int a = (rgb>>24)&0xFF;
		return new Tuple4IntIntIntInt(r, g, b, a);
	}

	private void saveRgb(String path, List<Integer> rgbs, int width, int height) throws IOException {

		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		for (int i = 0; i < height; i += 1) {
			for (int j = 0; j < width; j += 1) {
				image.setRGB(j, i, grayToRGB(rgbs.get(j + (i * width))));
			}
		}
		File outputImage = new File(path);
		ImageIO.write(image, "png", outputImage);

	}
	
	private void saveGray(String path, List<Integer> rgbs, int width, int height) throws IOException {

		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		for (int i = 0; i < height; i += 1) {
			for (int j = 0; j < width; j += 1) {
				image.setRGB(j, i, grayToRGB(rgbs.get(j + (i * width))));
			}
		}
		File outputImage = new File(path);
		ImageIO.write(image, "png", outputImage);

	}

}
