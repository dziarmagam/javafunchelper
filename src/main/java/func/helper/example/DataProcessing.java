package func.helper.example;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import func.helper.stream.BaseExtendedStream;
import func.helper.stream.window.Coordinate;
import func.helper.stream.window.Window;
import func.helper.stream.window.WindowFilters;
import func.helper.stream.window.WindowStreamUtils;
import func.helper.tuple.Tuple2;

public class DataProcessing {
	public static void main(String[] args) throws IOException {

//		long start = System.currentTimeMillis();
//		for (int i = 0; i < 10000; i++) {
//			getAverageExpenses(Paths.get("src/main/resources/expenses.csv"));
////			.forEach(
////					tuple -> System.out.println(tuple.value1 + " " + tuple.value2));
//		}
//		System.out.println(System.currentTimeMillis() - start);
//		start = System.currentTimeMillis();
//		for (int i = 0; i < 10000; i++) {
//			getAverageExpensesImperative(Paths.get("src/main/resources/expenses.csv"));
////			.forEach(
////					tuple -> System.out.println(tuple.value1 + " " + tuple.value2));
//		}
//		System.out.println(System.currentTimeMillis() - start);
		printValuesInColumn();
	}

	public static List<Tuple2<String, Double>> getAverageExpensesImperative(Path path) throws IOException {
		List<String> loadCsv = loadCsv(path);
		List<Tuple2<String, Double>> expensesWithNameList = new ArrayList<>();
		int columnWidth = 4;
		for (int i = 0; i < loadCsv.size(); i += columnWidth) {
			String name = loadCsv.get(i);
			double average = 0;
			for (int j = 1; j < columnWidth; j++) {
				average += Double.parseDouble(loadCsv.get(i + j));
			}
			average /= columnWidth - 1;
			expensesWithNameList.add(new Tuple2<>(name, average));
		}
		return expensesWithNameList;
	}
	
	public static void printValuesInColumn(){
		List<Integer> values = Arrays.asList(1,2,3,4,5,6);
		BaseExtendedStream.of(values)
		.window(2, Coordinate.ofAll(0,0 , 1,0))
//		.filter(WindowFilters.columnFilter(0))
		.forEach(window -> System.out.println(window.values));
	}

	public static List<Tuple2<String, Double>> getAverageExpenses(Path path) throws IOException {
		return BaseExtendedStream.of(loadCsv(path).stream())
				.window(4, Coordinate.ofRangeX(1, 4, 0).evaluate())
//				.parallel()
				.filter(WindowFilters.columnFilter(0))
				.map(window -> Window.of(parseIntegers(window), window.center, window.streamWidth))
				.map(window -> new Tuple2<>(window.center.value, averageValue(window)))
				.collect(Collectors.toList());
	}

	private static List<Double> parseIntegers(Window<String, String> window) {
		return window.values.stream().map(s -> Double.parseDouble(s)).collect(Collectors.toList());
	}

	private static double averageValue(Window<Double, String> window) {
		return window.values.stream().mapToDouble(i -> i).average().getAsDouble();
	}

	private static List<String> loadCsv(Path path) throws IOException {
		return Files.readAllLines(path).stream().flatMap(s -> Arrays.asList(s.split(",")).stream())
				.collect(Collectors.toList());
	}
}
