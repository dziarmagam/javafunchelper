import itertools

package = "package func.helper.tuple.primitive;"
imports = ["import java.util.Objects;"]

clazz = "public class "

types_table = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"]

primitive_types_table = ["long", "int", "double"]


def class_name(size, types, is_primitive):
    suffix = ""
    if is_primitive:
        suffix = "".join(first_letter_to_uppercase(types))
    return "Tuple" + str(size) + suffix


def generic(size):
    typoess = ""
    for x in range(0, size):
        typoess += types_table[x] + ","
    typoess = typoess[0:-1]
    return "<" + typoess + ">"


def format_imports(imports_var):
    string_imports = ""
    for imp0rt in imports_var:
        string_imports += imp0rt + "\n"
    return string_imports


def constructor(var_class_name, types_count, types_table_var):
    args = ""
    for x in range(0, types_count):
        args += types_table_var[x] + " value" + str(x + 1) + ", "
    args = args[0: -2]
    head = "public " + var_class_name + "(" + args + ") { \n"
    assigns = ""
    for x in range(0, types_count):
        assigns += "this.value" + str(x + 1) + " = value" + str(x + 1) + ";\n"
    body = "super(); \n " + assigns + "}"

    return head + body + "\n"


def hash_code(var_type_count):
    head = "@Override \n public int hashCode() {\n"
    values = ""
    for x in range(0, var_type_count):
        values += "value" + str(x + 1) + ", "
    values = values[0:-2]

    body = "return Objects.hash(" + values + ");\n}"
    return head + body


def equals(var_class_name, var_type_count):
    head = "@Override \n public boolean equals(Object obj){\n"
    body_common_part = \
        "if (this == obj)\n" + \
        "	return true; \n" + \
        "if (obj == null) \n" + \
        "	return false; \n" + \
        "if (getClass() != obj.getClass()) \n" + \
        "	return false; \n" + \
        var_class_name + " other = (" + var_class_name + ") obj;" + "\n"

    object_equals = ""
    for x in range(0, var_type_count):
        object_equals += "Objects.equals(this.value" + str(x + 1) + ",other.value" + str(x + 1) + ") && \n"
    object_equals = object_equals[0: -5]
    body = "return " + object_equals + ";"
    return head + body_common_part + "\n" + body + "\n}"


def fields(size, type_table):
    data = ""
    for x in range(0, size):
        data += "public final " + type_table[x] + " value" + str(x + 1) + ";\n"
    return data


def generate_class(class_name_var, size, is_primitve=False):
    if is_primitve:
        return package + "\n" + format_imports(imports) + "\n" + clazz + class_name_var + "{"
    else:
        return package + "\n" + format_imports(imports) + "\n" + clazz + class_name_var + generic(size) + "{"


def create_and_save_tuple(type_count, types_table_var, is_primitive=False):
    name = class_name(type_count, types_table_var, is_primitive)
    java_tuple = generate_class(name, type_count, is_primitive)
    java_tuple += fields(type_count, types_table_var)
    java_tuple += constructor(name, type_count, types_table_var)
    java_tuple += hash_code(type_count)
    java_tuple += equals(name, type_count)
    java_tuple += "}"
    text_io_wrapper = open(name + ".java", "w+")
    text_io_wrapper.write(java_tuple)


def first_letter_to_uppercase(list):
    uppercase_list = []
    for i in range(0, len(list)):
        string = list[i]
        uppercase_list.append(string[0].capitalize() + string[1:])
    return uppercase_list


for x in range(1, 11):
    create_and_save_tuple(x, types_table)
for i in range(1, 5):
    for y in itertools.product(primitive_types_table, repeat=i):
        create_and_save_tuple(len(y), y, True)

